﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WMPLib;
using System.Threading;
using System.Timers;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class Branch
        {
            public string access_token { get; set; }
        }

        public class Play
        {
            public string branch { get; set; }
            public string name { get; set; }
            public string songs { get; set; }
            public string spots { get; set; }
        }

        public System.Int32 volume { get; set; }
        public IWMPPlaylist currentPlaylist { get; set; }
        public WMPPlayState playState { get; set; }
        public System.String name { get; set; }

        HttpWebRequest req;
        HttpWebResponse res;
        Branch branch;
        StreamReader stmrd;
        public string token;
        int count_song = 0;
        int num_songs = 0;
        int now_song = 0;
        int index_time_spot = 0;
        int all_spots = 0;
        bool state = false;
        System.Timers.Timer timerx;

        string debug = "";
        private void button1_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel1.Visible = false;
            Thread.Sleep(100);
            Loading.Visible = true;
            Thread.Sleep(100);
            run_fnc();
        }
        public void offline_mode()
        {
            string temp_offline = "http://player.dittymusic.com/offline.mp3";
            wmp_offline.settings.volume = 0;
            wmp_offline2.settings.volume = 0;

            wmp_offline.URL = temp_offline;
            wmp_offline2.URL = temp_offline;
            wmp_offline.Ctlcontrols.play();
            wmp_offline2.Ctlcontrols.play();

            //WMPLib.WindowsMediaPlayer wmp_offline = new WMPLib.WindowsMediaPlayer();
            //WMPLib.WindowsMediaPlayer wmp_offline2 = new WMPLib.WindowsMediaPlayer();
            //wmp_offline.settings.volume = 0;
            //wmp_offline2.settings.volume = 0;
            //wmp_offline.URL = temp_offline;
            //wmp_offline2.URL = temp_offline;
            //wmp_offline.controls.play();
            //wmp_offline2.controls.play();

            //debug = debug + "In offline mode ||\n";
            //textBox3.Text = debug;
            ////string temp_offline = "http://player.dittymusic.com/offline.mp3";
            //wmp_offline.settings.volume = 0;
            //wmp_offline2.settings.volume = 0;
            //debug = debug + "create URL temp_offline ||\n";
            //textBox3.Text = debug;
            //WMPLib.IWMPPlaylist playlist = wmp_offline.playlistCollection.newPlaylist("myplaylist");
            //WMPLib.IWMPPlaylist playlist2 = wmp_offline2.playlistCollection.newPlaylist("myplaylist2");
            //debug = debug + "create playlist ||\n";
            //textBox3.Text = debug;
            //WMPLib.IWMPMedia media;
            //WMPLib.IWMPMedia media2;
            //debug = debug + "add media ||\n";
            //textBox3.Text = debug;
            //media = wmp_offline.newMedia(temp_offline);
            //media2 = wmp_offline2.newMedia(temp_offline);
            //debug = debug + "add url media ||\n";
            //textBox3.Text = debug;
            //playlist.appendItem(media);
            //playlist2.appendItem(media2);
            //debug = debug + "append Item ||\n";
            //textBox3.Text = debug;
            //wmp_offline.currentPlaylist = playlist;
            //wmp_offline2.currentPlaylist = playlist2;
            //debug = debug + "current Playlist ||\n";
            //textBox3.Text = debug;
            //wmp_offline.Ctlcontrols.play();
            //wmp_offline2.Ctlcontrols.play();
            //debug = debug + "paly song ||\n";
            //textBox3.Text = debug;
        }
        private void add_song_play_list(int num_songs)
        {
            //WMPLib.IWMPPlaylist playlist = media1_play.playlistCollection.newPlaylist("myplaylist");
            //WMPLib.IWMPMedia media;

            if (count_song == num_songs)
            {
                count_song = 0;
            }
            string temp_url = getmusic(count_song);
            if (temp_url != "0")
            {
                media1_play.URL = temp_url;
                media1_play.Ctlcontrols.play();
                //media = media1_play.newMedia(temp_url);
                //playlist.appendItem(media);

                //media1_play.currentPlaylist = playlist;
                //media1_play.Ctlcontrols.play();
                //textBox3.Text = count_song.ToString();
                count_song++;
            } else
            {
                wmp_offline.settings.volume = 0;
                wmp_offline2.Ctlcontrols.stop();
                wmp_offline2.Ctlcontrols.play();
                for (int i = 0; i <= 100; i = i + 5)
                {
                    wmp_offline2.settings.volume = i;
                    Thread.Sleep(100);
                }
                //temp_url = Path.GetFullPath(@"music.mp3");
                //media = media1_play.newMedia(temp_url);
                //playlist.appendItem(media);

                //media1_play.currentPlaylist = playlist;
                //media1_play.Ctlcontrols.play();
                //textBox3.Text = count_song.ToString();
            }
        }
        private void add_song_play_list2(int num_songs)
        {
            //WMPLib.IWMPPlaylist playlist = media2_play.playlistCollection.newPlaylist("myplaylist");
            //WMPLib.IWMPMedia media;

            if (count_song == num_songs)
            {
                count_song = 0;
            }
            string temp_url = getmusic(count_song);
            if (temp_url != "0")
            {
                media2_play.URL = temp_url;
                media2_play.Ctlcontrols.play();
                //media = media2_play.newMedia(temp_url);
                //playlist.appendItem(media);

                //media2_play.currentPlaylist = playlist;
                //media2_play.Ctlcontrols.play();
                //textBox3.Text = count_song.ToString();
                count_song++;
            } else
            {

                wmp_offline.settings.volume = 0;
                wmp_offline.Ctlcontrols.stop();
                wmp_offline.Ctlcontrols.play();
                for (int i = 0; i <= 100; i = i + 5)
                {
                    wmp_offline.settings.volume = i;
                    Thread.Sleep(100);
                }
                //temp_url = Path.GetFullPath(@"music.mp3");
                //media = media2_play.newMedia(temp_url);
                //playlist.appendItem(media);

                //media2_play.currentPlaylist = playlist;
                //media2_play.Ctlcontrols.play();
                //textBox3.Text = count_song.ToString();
            }
        }

        ArrayList list_name = new ArrayList();
        ArrayList list_artist = new ArrayList();
        public string getmusic(int song_id)
        {
            string URLmp3;
            try
            {
                string song_id1 = (string)json.SelectToken("songs[" + song_id.ToString() + "].id");
                req = (HttpWebRequest)WebRequest.Create("http://app.dittymusic.com/api/songs/" + song_id1 + "/stream");
                req.Credentials = CredentialCache.DefaultCredentials;
                req.Method = "GET";
                req.PreAuthenticate = true;
                req.Headers.Add("Authorization", "bearer " + branch.access_token);
                req.ContentType = "application/x-www-form-urlencoded";

                res = (HttpWebResponse)req.GetResponse();
                Stream receiveSteam3 = res.GetResponseStream();
                stmrd = new StreamReader(receiveSteam3, Encoding.UTF8);

                URLmp3 = stmrd.ReadToEnd();

                res.Close();
                stmrd.Close();
                list_name.Add((string)json.SelectToken("songs[" + song_id.ToString() + "].title"));
                list_artist.Add((string)json.SelectToken("songs[" + song_id.ToString() + "].artist"));
                lbl_.Text = "";
                //lbl_.Visible = false;
            }
            catch (Exception ex)
            {
                URLmp3 = "0";
                lbl_.Text = ex.ToString();
                //lbl_.Visible = true;
            }
            return URLmp3;
        }

        public string getspot(int spot_id)
        {
            string URLmp3;
            try
            {
                //string spot_id1 = (string)json.SelectToken("spot[" + song_id.ToString() + "].id");
                req = (HttpWebRequest)WebRequest.Create("http://app.dittymusic.com/api/spots/" + spot_id.ToString() + "/stream");
                req.Credentials = CredentialCache.DefaultCredentials;
                req.Method = "GET";
                req.PreAuthenticate = true;
                req.Headers.Add("Authorization", "bearer " + branch.access_token);
                req.ContentType = "application/x-www-form-urlencoded";

                res = (HttpWebResponse)req.GetResponse();
                Stream receiveSteam3 = res.GetResponseStream();
                stmrd = new StreamReader(receiveSteam3, Encoding.UTF8);

                URLmp3 = stmrd.ReadToEnd();

                res.Close();
                stmrd.Close();
                lbl_.Text = "";
                //lbl_.Visible = false;
            } catch (Exception ex)
            {
                URLmp3 = "0";
                lbl_.Text = ex.ToString();
                //lbl_.Visible = true;
            }
            return URLmp3;
        }

        JToken json;
        ArrayList list_timespots = new ArrayList();
        ArrayList list_spots = new ArrayList();
        public int getlist(string token)
        {
            debug = debug + "in getlist ||\n";
            req = (HttpWebRequest)WebRequest.Create("http://app.dittymusic.com/api/player");
            debug = debug + "Create webRequest ||\n";
            req.Credentials = CredentialCache.DefaultCredentials;
            req.Method = "GET";
            req.PreAuthenticate = true;
            req.Headers.Add("Authorization", "bearer " + token);
            req.ContentType = "application/x-www-form-urlencoded";
            debug = debug + "Before Get ||\n";
            res = (HttpWebResponse)req.GetResponse();
            debug = debug + "recive res is response ||\n";
            Stream receiveSteam2 = res.GetResponseStream();
            stmrd = new StreamReader(receiveSteam2, Encoding.UTF8);
            string jsonLoad2 = stmrd.ReadToEnd();
            debug = debug + "read json ||\n";
            json = JObject.Parse(jsonLoad2);


            int x = json["songs"].Count();
            all_spots = json["spots"].Count();
            string temp_time;

            for (int j = 0; j < 24; j++)
            {
                temp_time = (string)(json.SelectToken("spots." + (j).ToString() + ".pivot.time"));
                if (temp_time == null)
                {

                } else
                {
                    list_spots.Add((int)(json.SelectToken("spots." + (j).ToString() + ".id")));
                    DateTime tmp_time = DateTime.Parse(temp_time + ":00:00");
                    list_timespots.Add(tmp_time);
                }
            }

            //debug
            DateTime currenTime = DateTime.Now;
            string str = "";
            int i;
            for (i = 0; i < list_timespots.Count; i++)
            {
                str = list_timespots[i].ToString();
                DateTime userTime = DateTime.Parse(str);
                if (userTime.Hour > currenTime.Hour)
                {
                    index_time_spot = i;
                    break;
                }
            }

            //string logtest = textBox1.Text.ToString();
            //if (logtest == "")
            //{

            //}
            //else
            //{
            //    DateTime tmpss = DateTime.Parse(logtest);
            //    list_timespots[i + 1] = tmpss;
            //}
            //DateTime tmpss = DateTime.Parse(logtest);
            //list_timespots[i + 1] = tmpss;
            //tmpss = DateTime.Parse("22:58:00");
            //list_timespots[i + 2] = tmpss;
            //tmpss = DateTime.Parse("23:01:00");
            //list_timespots[i + 3] = tmpss;
            //debug
            //for (int i = 0; i < list_timespots.Count; i++)
            //{
            //    textBox2.Text = textBox2.Text + "\n" + list_timespots[i].ToString();
            //}
            //textBox2.Text = "Next Spot: " + list_timespots[index_time_spot].ToString();
            string brach_name = (string)json.SelectToken("branch.name");
            tb_branch.Text = brach_name;

            res.Close();
            stmrd.Close();
            return x-1;
        }

        public string gettoken()
        {
            string str_token = "";
            req = (HttpWebRequest)WebRequest.Create("http://app.dittymusic.com/api/auth/login/");
            req.MaximumAutomaticRedirections = 4;
            req.MaximumResponseHeadersLength = 4;
            req.Credentials = CredentialCache.DefaultCredentials;
            req.Method = "POST";
            req.ContentType = "application/json";
            StreamWriter stmwr = new StreamWriter(req.GetRequestStream());

            string json = "{\"account_type\":\"branch\"," +
                            "\"password\":\"" + tbx_code.Text + "\"}";
            stmwr.Write(json);
            stmwr.Flush();
            stmwr.Close();
            try
            {
                res = (HttpWebResponse)req.GetResponse();

                Stream receiveStream = res.GetResponseStream();

                stmrd = new StreamReader(receiveStream, Encoding.UTF8);
                String jsonLoad = stmrd.ReadToEnd();

                branch = JsonConvert.DeserializeObject<Branch>(jsonLoad);

                res.Close();
                stmrd.Close();
                str_token = branch.access_token;
                lbl_er.Text = "";
                //lbl_er.Visible = false;
            } catch (Exception ex)
            {
                if (CheckForInternetConnection())
                {
                    lbl_er.Text = "Invalid User or Password";
                } else
                {
                    lbl_er.Text = ex.ToString();
                }
                //lbl_er.Visible = true;
            }
            return str_token;
        }
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("https://www.google.co.th/"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int description, int reservedValue);
        public static bool IsInternetAvailable()
        {
            try
            {
                int description;
                return InternetGetConnectedState(out description, 0);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool flg_net = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            lbl_time.Text = DateTime.Now.ToString("HH:mm:ss");
            textBox3.Text = debug;
            bool connection = IsInternetAvailable();
            if (connection == false && flg_net == true)
            {
                flg_net = false;
                // background workker
                //bgWorker.RunWorkerAsync();
                test_net();
            } else
            {
                flg_net = true;
            }
        }

        private void test_net()
        {
            if (media1_play.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                for (int i = 100; i >= 0; i = i - 5)
                {
                    media1_play.settings.volume = i;
                    Thread.Sleep(100);
                }
                media1_play.Ctlcontrols.stop();
                media1_play.settings.volume = 100;
            }
            else if (media2_play.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                for (int i = 100; i >= 0; i = i - 5)
                {
                    media2_play.settings.volume = i;
                    Thread.Sleep(100);
                }
                media2_play.Ctlcontrols.stop();
                media2_play.settings.volume = 100;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label2.Visible = false;
            textBox1.Visible = false;
            lbl_debug.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            media1_play.Visible = false;
            media2_play.Visible = false;
            wmp_spot.Visible = false;
            wmp_offline.Visible = false;
            wmp_offline2.Visible = false;

            timer1.Start();
            media1_play.settings.volume = 100;
            media2_play.settings.volume = 100;
            wmp_spot.settings.volume = 100;
            wmp_offline.settings.volume = 0;
            wmp_offline.settings.volume = 0;

            timerx = new System.Timers.Timer();
            timerx.Interval = 1000;
            timerx.Elapsed += Timer_Elapsed;
            timerx.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            DateTime currenTime = DateTime.Now;
            string str = list_timespots[index_time_spot].ToString();
            DateTime userTime = DateTime.Parse(str);

            if (currenTime.Hour == userTime.Hour && currenTime.Minute == userTime.Minute && currenTime.Second == currenTime.Second)
            {
                timerx.Stop();
                try
                {
                    play_spot(index_time_spot);
                    index_time_spot++;
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        public void play_spot(int index_time_spot)
        {
            if (index_time_spot >= all_spots)
            {
                index_time_spot = 0;
            }
            string index = list_spots[index_time_spot].ToString();
            string temp_url = getspot(int.Parse(index));
            //MessageBox.Show(index + "####" + temp_url);
            if (temp_url != "0")
            {
                wmp_spot.Ctlcontrols.stop();
                wmp_spot.URL = temp_url;
                if (state)
                {
                    for (int i = 100; i >= 0; i = i - 5)
                    {
                        media2_play.settings.volume = i;
                        Thread.Sleep(100);
                    }
                    media2_play.Ctlcontrols.pause();
                }
                else
                {
                    for (int i = 100; i >= 0; i = i - 5)
                    {
                        media1_play.settings.volume = i;
                        Thread.Sleep(100);
                    }
                    media1_play.Ctlcontrols.pause();
                }
                wmp_spot.Ctlcontrols.play();
            }
        }

        private void wmp_spot_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            // || e.newState == 8
            if (e.newState == 1)
            {
                if (state)
                {
                    media2_play.Ctlcontrols.play();
                    for (int i = 0; i <= 100; i = i + 5)
                    {
                        media2_play.settings.volume = i;
                        Thread.Sleep(100);
                    }
                }
                else
                {
                    media1_play.Ctlcontrols.play();
                    for (int i = 0; i <= 100; i = i + 5)
                    {
                        media1_play.settings.volume = i;
                        Thread.Sleep(100);
                    }
                }
                timerx.Start();
            }
        }

        private void media_play_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 8 || e.newState == 1)
            {
                add_song_play_list2(num_songs);
                now_song++;
                //textBox2.Text = now_song.ToString();
                tb_songname.Text = list_name[now_song].ToString();
                tb_artistname.Text = list_artist[now_song].ToString();
                state = true;
            }
        }

        private void media2_play_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 8 || e.newState == 1)
            {
                add_song_play_list(num_songs);
                now_song++;
                //textBox2.Text = now_song.ToString();
                tb_songname.Text = list_name[now_song].ToString();
                tb_artistname.Text = list_artist[now_song].ToString();
                state = false;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
            Application.Exit();
        }

        private void wmp_offline_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 8 || e.newState == 1)
            {

                if ((media2_play.playState != WMPLib.WMPPlayState.wmppsPlaying) && (media1_play.playState != WMPLib.WMPPlayState.wmppsPlaying))
                {
                    if (state)
                    {
                        add_song_play_list2(num_songs);
                    }
                    else
                    {
                        add_song_play_list(num_songs);
                    }
                }
            }
        }

        private void wmp_offline2_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 8 || e.newState == 1)
            {
                if ((media2_play.playState != WMPLib.WMPPlayState.wmppsPlaying) && (media1_play.playState != WMPLib.WMPPlayState.wmppsPlaying))
                {
                    if (state)
                    {
                        add_song_play_list2(num_songs);
                    }
                    else
                    {
                        add_song_play_list(num_songs);
                    }
                }
            }
        }

        public void run_fnc()
        {
            debug = debug + "Start get token ||\n";
            string str_token = gettoken();
            debug = debug + "gettoken Pass ||\n";
            if (str_token != "")
            {
                //pass
                offline_mode();
                debug = debug + "offline_mode Pass ||\n";
                num_songs = getlist(str_token);
                debug = debug + "get list song Pass ||\n";

                add_song_play_list(num_songs);
                debug = debug + "add song to play list Pass ||\n";

                tb_songname.Text = list_name[now_song].ToString();
                tb_artistname.Text = list_artist[now_song].ToString();
                lbl_debug.Text = num_songs.ToString();
                debug = debug + "Pring detail Pass ||\n";
                panel2.Visible = true;
            }
            else
            {
                //fail
                panel2.Visible = false;
                panel1.Visible = true;
            }
            Loading.Visible = false;
        }
    }
}

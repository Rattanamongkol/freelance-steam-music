﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_er = new System.Windows.Forms.Label();
            this.lbl_error = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tbx_code = new System.Windows.Forms.TextBox();
            this.lbl_login = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_ = new System.Windows.Forms.Label();
            this.tb_branch = new System.Windows.Forms.TextBox();
            this.tb_artistname = new System.Windows.Forms.TextBox();
            this.tb_songname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_artistname = new System.Windows.Forms.Label();
            this.lbl_songname = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbl_time = new System.Windows.Forms.Label();
            this.lbl_debug = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.media1_play = new AxWMPLib.AxWindowsMediaPlayer();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.wmp_spot = new AxWMPLib.AxWindowsMediaPlayer();
            this.media2_play = new AxWMPLib.AxWindowsMediaPlayer();
            this.wmp_offline = new AxWMPLib.AxWindowsMediaPlayer();
            this.wmp_offline2 = new AxWMPLib.AxWindowsMediaPlayer();
            this.Loading = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.media1_play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_spot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.media2_play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_offline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_offline2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel1.Controls.Add(this.lbl_er);
            this.panel1.Controls.Add(this.lbl_error);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.tbx_code);
            this.panel1.Controls.Add(this.lbl_login);
            this.panel1.Location = new System.Drawing.Point(190, 127);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(458, 225);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // lbl_er
            // 
            this.lbl_er.Font = new System.Drawing.Font("Dubai", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_er.ForeColor = System.Drawing.Color.Brown;
            this.lbl_er.Location = new System.Drawing.Point(32, 89);
            this.lbl_er.Name = "lbl_er";
            this.lbl_er.Size = new System.Drawing.Size(394, 23);
            this.lbl_er.TabIndex = 10;
            this.lbl_er.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.ForeColor = System.Drawing.Color.OrangeRed;
            this.lbl_error.Location = new System.Drawing.Point(18, 96);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(0, 13);
            this.lbl_error.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(229)))), ((int)(((byte)(146)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Dubai", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(16, 162);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(428, 48);
            this.button1.TabIndex = 2;
            this.button1.Text = "Launch Player";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbx_code
            // 
            this.tbx_code.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(40)))), ((int)(((byte)(48)))));
            this.tbx_code.Font = new System.Drawing.Font("Dubai Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbx_code.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.tbx_code.Location = new System.Drawing.Point(16, 115);
            this.tbx_code.Name = "tbx_code";
            this.tbx_code.PasswordChar = '*';
            this.tbx_code.Size = new System.Drawing.Size(428, 44);
            this.tbx_code.TabIndex = 1;
            this.tbx_code.Text = "12345678";
            this.tbx_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_login
            // 
            this.lbl_login.AutoSize = true;
            this.lbl_login.Font = new System.Drawing.Font("Dubai Medium", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_login.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(40)))), ((int)(((byte)(48)))));
            this.lbl_login.Location = new System.Drawing.Point(122, 28);
            this.lbl_login.Name = "lbl_login";
            this.lbl_login.Size = new System.Drawing.Size(238, 67);
            this.lbl_login.TabIndex = 0;
            this.lbl_login.Text = "Please Login";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel2.Controls.Add(this.lbl_);
            this.panel2.Controls.Add(this.tb_branch);
            this.panel2.Controls.Add(this.tb_artistname);
            this.panel2.Controls.Add(this.tb_songname);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lbl_artistname);
            this.panel2.Controls.Add(this.lbl_songname);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Location = new System.Drawing.Point(190, 127);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(458, 225);
            this.panel2.TabIndex = 4;
            this.panel2.Visible = false;
            // 
            // lbl_
            // 
            this.lbl_.Font = new System.Drawing.Font("Dubai", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_.ForeColor = System.Drawing.Color.Brown;
            this.lbl_.Location = new System.Drawing.Point(50, 206);
            this.lbl_.Name = "lbl_";
            this.lbl_.Size = new System.Drawing.Size(394, 23);
            this.lbl_.TabIndex = 9;
            this.lbl_.Visible = false;
            // 
            // tb_branch
            // 
            this.tb_branch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(214)))));
            this.tb_branch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_branch.Font = new System.Drawing.Font("Dubai Medium", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_branch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(62)))), ((int)(((byte)(113)))));
            this.tb_branch.Location = new System.Drawing.Point(16, 28);
            this.tb_branch.Name = "tb_branch";
            this.tb_branch.ReadOnly = true;
            this.tb_branch.Size = new System.Drawing.Size(424, 41);
            this.tb_branch.TabIndex = 8;
            this.tb_branch.Text = "Name Song";
            this.tb_branch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_artistname
            // 
            this.tb_artistname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.tb_artistname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_artistname.Font = new System.Drawing.Font("Microsoft PhagsPa", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_artistname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(62)))), ((int)(((byte)(113)))));
            this.tb_artistname.Location = new System.Drawing.Point(14, 183);
            this.tb_artistname.Name = "tb_artistname";
            this.tb_artistname.ReadOnly = true;
            this.tb_artistname.Size = new System.Drawing.Size(428, 21);
            this.tb_artistname.TabIndex = 7;
            this.tb_artistname.Text = "Name Song";
            this.tb_artistname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_songname
            // 
            this.tb_songname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.tb_songname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_songname.Font = new System.Drawing.Font("Microsoft PhagsPa", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_songname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(62)))), ((int)(((byte)(113)))));
            this.tb_songname.Location = new System.Drawing.Point(14, 152);
            this.tb_songname.Name = "tb_songname";
            this.tb_songname.ReadOnly = true;
            this.tb_songname.Size = new System.Drawing.Size(428, 26);
            this.tb_songname.TabIndex = 6;
            this.tb_songname.Text = "Name Song";
            this.tb_songname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Dubai", 8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(62)))), ((int)(((byte)(113)))));
            this.label1.Location = new System.Drawing.Point(3, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Logout";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbl_artistname
            // 
            this.lbl_artistname.AutoSize = true;
            this.lbl_artistname.Font = new System.Drawing.Font("Dubai", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_artistname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(62)))), ((int)(((byte)(113)))));
            this.lbl_artistname.Location = new System.Drawing.Point(184, 182);
            this.lbl_artistname.Name = "lbl_artistname";
            this.lbl_artistname.Size = new System.Drawing.Size(89, 27);
            this.lbl_artistname.TabIndex = 3;
            this.lbl_artistname.Text = "Name artist";
            this.lbl_artistname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_songname
            // 
            this.lbl_songname.AutoSize = true;
            this.lbl_songname.Font = new System.Drawing.Font("Microsoft PhagsPa", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_songname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(62)))), ((int)(((byte)(113)))));
            this.lbl_songname.Location = new System.Drawing.Point(174, 152);
            this.lbl_songname.Name = "lbl_songname";
            this.lbl_songname.Size = new System.Drawing.Size(108, 24);
            this.lbl_songname.TabIndex = 2;
            this.lbl_songname.Text = "Name Song";
            this.lbl_songname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(62)))), ((int)(((byte)(113)))));
            this.panel4.Controls.Add(this.lbl_time);
            this.panel4.Location = new System.Drawing.Point(14, 83);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(428, 52);
            this.panel4.TabIndex = 1;
            // 
            // lbl_time
            // 
            this.lbl_time.AutoSize = true;
            this.lbl_time.Font = new System.Drawing.Font("Dubai Medium", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_time.ForeColor = System.Drawing.Color.White;
            this.lbl_time.Location = new System.Drawing.Point(157, 7);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(105, 40);
            this.lbl_time.TabIndex = 0;
            this.lbl_time.Text = "00:00:00";
            this.lbl_time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_debug
            // 
            this.lbl_debug.AllowDrop = true;
            this.lbl_debug.AutoSize = true;
            this.lbl_debug.Location = new System.Drawing.Point(9, 131);
            this.lbl_debug.Name = "lbl_debug";
            this.lbl_debug.Size = new System.Drawing.Size(59, 13);
            this.lbl_debug.TabIndex = 5;
            this.lbl_debug.Text = "lbl_debug1";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(317, 14);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(299, 104);
            this.textBox2.TabIndex = 6;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(12, 20);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(299, 98);
            this.textBox3.TabIndex = 7;
            // 
            // media1_play
            // 
            this.media1_play.Enabled = true;
            this.media1_play.Location = new System.Drawing.Point(17, 371);
            this.media1_play.Name = "media1_play";
            this.media1_play.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("media1_play.OcxState")));
            this.media1_play.Size = new System.Drawing.Size(343, 63);
            this.media1_play.TabIndex = 8;
            this.media1_play.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.media_play_PlayStateChange);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // wmp_spot
            // 
            this.wmp_spot.Enabled = true;
            this.wmp_spot.Location = new System.Drawing.Point(12, 457);
            this.wmp_spot.Name = "wmp_spot";
            this.wmp_spot.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmp_spot.OcxState")));
            this.wmp_spot.Size = new System.Drawing.Size(348, 61);
            this.wmp_spot.TabIndex = 12;
            this.wmp_spot.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.wmp_spot_PlayStateChange);
            // 
            // media2_play
            // 
            this.media2_play.Enabled = true;
            this.media2_play.Location = new System.Drawing.Point(379, 371);
            this.media2_play.Name = "media2_play";
            this.media2_play.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("media2_play.OcxState")));
            this.media2_play.Size = new System.Drawing.Size(400, 63);
            this.media2_play.TabIndex = 14;
            this.media2_play.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.media2_play_PlayStateChange);
            // 
            // wmp_offline
            // 
            this.wmp_offline.Enabled = true;
            this.wmp_offline.Location = new System.Drawing.Point(379, 457);
            this.wmp_offline.Name = "wmp_offline";
            this.wmp_offline.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmp_offline.OcxState")));
            this.wmp_offline.Size = new System.Drawing.Size(208, 61);
            this.wmp_offline.TabIndex = 15;
            this.wmp_offline.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.wmp_offline_PlayStateChange);
            // 
            // wmp_offline2
            // 
            this.wmp_offline2.Enabled = true;
            this.wmp_offline2.Location = new System.Drawing.Point(593, 457);
            this.wmp_offline2.Name = "wmp_offline2";
            this.wmp_offline2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmp_offline2.OcxState")));
            this.wmp_offline2.Size = new System.Drawing.Size(208, 61);
            this.wmp_offline2.TabIndex = 16;
            this.wmp_offline2.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.wmp_offline2_PlayStateChange);
            // 
            // Loading
            // 
            this.Loading.AutoSize = true;
            this.Loading.Font = new System.Drawing.Font("Dubai Medium", 18F);
            this.Loading.ForeColor = System.Drawing.Color.Turquoise;
            this.Loading.Location = new System.Drawing.Point(356, 352);
            this.Loading.Name = "Loading";
            this.Loading.Size = new System.Drawing.Size(113, 40);
            this.Loading.TabIndex = 17;
            this.Loading.Text = "Loading...";
            this.Loading.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(642, 78);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(639, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "INPUT TIME (HH:MM:SS)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(51)))), ((int)(((byte)(62)))));
            this.ClientSize = new System.Drawing.Size(842, 514);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.wmp_offline2);
            this.Controls.Add(this.wmp_offline);
            this.Controls.Add(this.media2_play);
            this.Controls.Add(this.wmp_spot);
            this.Controls.Add(this.media1_play);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.lbl_debug);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Loading);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(858, 553);
            this.MinimumSize = new System.Drawing.Size(858, 553);
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ditty Player";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.media1_play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_spot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.media2_play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_offline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmp_offline2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbx_code;
        private System.Windows.Forms.Label lbl_login;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbl_artistname;
        private System.Windows.Forms.Label lbl_songname;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.Label lbl_debug;
        private System.Windows.Forms.TextBox textBox3;
        private AxWMPLib.AxWindowsMediaPlayer media1_play;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private AxWMPLib.AxWindowsMediaPlayer wmp_spot;
        private System.Windows.Forms.TextBox tb_songname;
        private System.Windows.Forms.TextBox tb_artistname;
        private System.Windows.Forms.TextBox tb_branch;
        private AxWMPLib.AxWindowsMediaPlayer media2_play;
        private System.Windows.Forms.Label lbl_;
        private System.Windows.Forms.Label lbl_er;
        private AxWMPLib.AxWindowsMediaPlayer wmp_offline;
        private AxWMPLib.AxWindowsMediaPlayer wmp_offline2;
        public System.Windows.Forms.Label Loading;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textBox2;
    }
}

